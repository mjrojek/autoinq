from window_states import WindowState
from typ_variables import TYP23_vars
import time


class TestC1(WindowState):
    def __init__(self):
        super().__init__()
        print("INSIDE TEST C1")
        self.make_shell("0000020")
        time.sleep(5)
        self.create_typ22()


class TestC2(WindowState):
    def __init__(self):
        super().__init__()
        print("INSIDE TEST C2")
        self.create_typ22()


class C280840(WindowState):
    def __init__(self):
        super().__init__()
        print("INSIDE TEST C280840")
        # self.create_typ22()


class StaffPass(WindowState):
    def __init__(self):
        super().__init__()
        print("Making StaffPass")
        self.make_shell("0000020")
        time.sleep(5)
        self.create_typ22()


class Test(WindowState):
    def __init__(self, ipe_vars):
        super().__init__()
        print("Making Test")
        # Making shell is working
        # self.make_shell(ipe_vars.var_issn)
        # self.create_typ22("1F", 1)
        self.create_typ23(ipe_vars)


class C280849(WindowState):
    def __init__(self, ipe_vars, typ_vars, checkbutton_variable):
        super().__init__()
        print("INSIDE TEST C280852")
        if checkbutton_variable == 1:
            self.make_shell(ipe_vars.var_issn)
        self.create_typ23(ipe_vars, typ_vars, checkbutton_variable)


class C280850(WindowState):
    def __init__(self, ipe_vars, typ_vars, checkbutton_variable):
        super().__init__()
        print("INSIDE TEST C280850")
        if checkbutton_variable == 1:
            self.make_shell(ipe_vars.var_issn)
        self.create_typ23(ipe_vars, typ_vars)


class C280851(WindowState):
    def __init__(self, ipe_vars, typ_vars, checkbutton_variable):
        super().__init__()
        print("INSIDE TEST C280851")
        if checkbutton_variable == 1:
            self.make_shell(ipe_vars.var_issn)
        self.create_typ23(ipe_vars, typ_vars)


