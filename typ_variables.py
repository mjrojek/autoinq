# class IPEVariables:
#     def __init__(self,
#                  var_ptyp,
#                  var_orig_nlc,
#                  var_dest_nlc,
#                  var_rtcode):
#
#         self.var_ptyp = var_ptyp
#         self.var_orig_nlc = var_orig_nlc
#         self.var_dest_nlc = var_dest_nlc
#         self.var_rtcode = var_rtcode
#
#         # Will be used later
#         self.var_count_transfers
#         self.var_autorenew

class IPEVariables:
    def __init__(self):

        self.var_issn = None
        self.var_orig_nlc = None
        self.var_dest_nlc = None
        self.var_rtcode = None

        self.var_ptyp = None
        self.var_ipe_exp_date = None


class TYP22_vars():
    def __init__(self):
        self.var_count_journeys_remaining = None
        self.ExpiryDate = None
        self.ExpiryDate_sp = None

class TYP23_vars():
    def __init__(self):
        self.var_count_journeys_remaining = None


class TYP24_vars(IPEVariables):
    def __init__(self):
        super().__init__()
        #TODO: add typ24 specific variables