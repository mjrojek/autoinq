from pywinauto.application import Application
from helper_functions import rand_N_digits
import time
from typ_variables import  *

# TODO: EXTRACTING VLAUES OF ISSN AND OTHER VARIABLES !!
# TODO: Clean card [] tickbox ? - if you want to remove shell at the same time

# ORDER OF WRITING A SHELL TO THE CARD:
# 1.bind to project
# 2. wipe card
# 3. check cm_type
# 4. enter_issn
# 5. write_shell_step
# This is implemented in one function: make_shell()

# Number down clicks for each TYP
# 4: 14
# 5: 16
# 8: 22
# 10: 23
# 11: 24
class WindowState:
    def __init__(self):

        self.app = Application()
        self.main_window = None
        self.CMTool_Editor = None
        self.IPE_dev = None

# ----------------------------------------------------------------------------------------------------------------------
# SHELL FUNCTIONS START
    def bind(self, event=None):
        # When Project has not been loaded yet.
        self.app.Connect(title=u'INQ Test Tool', class_name='ThunderRT6MDIForm')

    def bind_to_project(self):
        self.app.Connect(title=u'ITSO CM Data Generator And Test Tool 2.1.4.16', class_name='ThunderRT6MDIForm')
        self.main_window = self.app[u'ITSO CM Data Generator And Test Tool 2.1.4.16']

    def wipe_card(self):
        self.main_window.MenuItem(u'&Tools->F&VC7 Delete Application').Click()
        print("wiped card")

    def check_cm_type(self):
        # CM Viewer
        self.main_window.MenuItem(u'&Tools->CM &Viewer').Click()
        self.main_window.ITSO_Customer_Media_Viewer.Clear.Click()

        self.main_window.MenuItem(u'&Tools->CM &Generator').Click()

        # Check if 07 has been already set
        if self.main_window.ITSO_Customer_Media_Generator.Edit1.texts()[1] != '07':
            self.main_window.ITSO_Customer_Media_Generator.Edit1.Click()
            self.main_window.ITSO_Customer_Media_Generator.Edit1.type_keys('{DOWN 6}{ENTER}')

        self.main_window.ITSO_Customer_Media_Generator.New_CM.Click()
        self.main_window.ITSO_Customer_Media_Generator.TTreeControl.RightClick()
        self.main_window.ITSO_Customer_Media_Generator.TTreeControl.type_keys('{END}{ENTER}')

    def enter_issn(self,issn):
        # Entering new ISSN
        self.CMTool_Editor = self.app[u'Editor']
        self.CMTool_Editor.TTreeControl.RightClick()

        # Go to ISSN
        self.CMTool_Editor.TTreeControl.type_keys('II')

        # Enter ISSN
        self.CMTool_Editor.Edit.Click()
        self.CMTool_Editor.Edit.DoubleClick()  # WHY click then double click.....

        if issn == "":
            self.CMTool_Editor.Edit.type_keys(rand_N_digits(7))
            self.CMTool_Editor.Edit.type_keys('{ENTER}')
        else:
            self.CMTool_Editor.Edit.type_keys(issn)
            self.CMTool_Editor.Edit.type_keys('{ENTER}')
        # time.sleep(1)
        self.CMTool_Editor.OK.Click()

    def write_shell_step(self):
        self.main_window.ITSO_Customer_Media_Generator.Create_Image.Click()

        # wait for popup OK
        popup = self.app.Dialog
        popup.Wait('ready')
        popup[u'&Yes'].Click()
        print("1 WINDOW POP UP - OK - do you want to create ITSO APP ?")

        # Click OK - Image Created.
        popup2 = self.app.Dialog
        popup2.Wait('ready', timeout=15)
        popup2.OK.Click()
        print("2 WINDOW POP UP - OK - image created - processing fnished")

        # Press Write CM - Writing a shell to Card
        self.main_window.ITSO_Customer_Media_Generator.Write_CM.Click()
        print("3 WINDOW POP UP - OK - Written to the card")

    def make_shell(self, issn):
        self.bind_to_project()
        self.wipe_card()
        self.check_cm_type()
        self.enter_issn(issn)
        self.write_shell_step()

# SHELL FUNCTIONS END
# ----------------------------------------------------------------------------------------------------------------------

# IPE CREATION FUNCTIONS
# ORDER of making IPE:
# Bind to project, select directory, select top IPE,

    def select_first_memory_struct(self):
        # TODO: Implement swticher
        # Highlight desired field (at the moment, one above Shell Environment)
        self.main_window.ITSO_Customer_Media_Generator.TTreeControl0.RightClick()
        self.main_window.ITSO_Customer_Media_Generator.TTreeControl0.type_keys('{DOWN 15}{UP}')
        # Maybe {PGDN} ?

    def select_top_ipe(self):
        # Okay, the following 2 lines took me an hour...
        self.main_window.ITSO_Customer_Media_Generator.IPECollection.ClickInput()
        self.IPE_dev = self.main_window.ITSO_Customer_Media_Generator.ThunderRT6ListBox.type_keys('{HOME}')
        print("CLICK on the list ")

    def set_ptyp(self, CMTool, value):
        CMTool.TTreeControl0.type_keys('P')
        CMTool.Edit.Click()
        CMTool.Edit.DoubleClick()  # WHY click then double click.....
        CMTool.Edit.type_keys(value)
        CMTool.Edit.type_keys('{ENTER}')


# Functions for Creating products
# ----------------------------------------------------------------------------------------------------------------------
    def create_typ14(self):
        pass

    def create_typ16(self):
        pass

    def create_typ22(self, ptyp, VG):
        # TODO: typ22 is only for staff pass at the moment - ned to add other steps and variables

        self.bind_to_project()
        self.select_first_memory_struct()
        self.select_top_ipe()
        self.IPE_dev.type_keys('{DOWN 8}')
        self.main_window.ITSO_Customer_Media_Generator.ð2.Click()
        IPE_set_window = self.app[u'IPE 22 (Rev 2) Settings']

        checkbox_value_groups = IPE_set_window.child_window(title="Value Groups",
                                                            class_name="ThunderRT6CheckBox")
        checkbox_value_groups_state = checkbox_value_groups.GetCheckState()
        # TICK VG for VG=1 variable  (adult season)
        # TODO:  i made some changes here with VG
        if VG == 1:
            if checkbox_value_groups_state == 0:
                checkbox_value_groups_state.Click()
        else:
            if checkbox_value_groups_state == 1:
                checkbox_value_groups_state.Click()

        # ALT + I - IIN as a reference
        # IPE_set_window.type_keys('%i')
        # IPE_set_window.type_keys('{TAB 5}{SPACE}')

        IPE_set_window.SetFocus()
        IPE_set_window.OK.Click()
        self.select_first_memory_struct()
        self.main_window.ITSO_Customer_Media_Generator.TTreeControl.type_keys('{ENTER}')

        self.CMTool_Editor = self.app[u'Editor']
        self.CMTool_Editor.TTreeControl.RightClick()

        # Set PTYP
        self.CMTool_Editor.TTreeControl0.type_keys('P')
        self.CMTool_Editor.Edit.Click()
        self.CMTool_Editor.Edit.DoubleClick()  # WHY click then double click.....
        self.CMTool_Editor.Edit.type_keys(ptyp)
        self.CMTool_Editor.Edit.type_keys('{ENTER}')

        # Set expiry date
        self.CMTool_Editor.TTreeControl0.RightClickInput()
        self.CMTool_Editor.TTreeControl0.type_keys('EEE')
        self.CMTool_Editor.Edit.Click()
        self.CMTool_Editor.Edit.DoubleClick()  # Double Click = Select current content
        self.CMTool_Editor.Edit.type_keys('2442{ENTER}')

        self.CMTool_Editor.SetFocus()
        self.CMTool_Editor.OK.Click()
        self.main_window.ITSO_Customer_Media_Generator.Create_Image.Click()

        popup2 = self.app.Dialog
        popup2.Wait('ready', timeout=18)
        popup2.OK.Click()

        self.main_window.ITSO_Customer_Media_Generator.Write_CM.Click()

    def create_typ23(self, ipe_vars, typ_vars):

        if self.main_window == None:
            self.bind_to_project()

        self.select_first_memory_struct()
        self.select_top_ipe()
        self.IPE_dev.type_keys('{DOWN 10}')
        self.main_window.ITSO_Customer_Media_Generator.ð2.Click()
        IPE_set_window = self.app[u'IPE 23 (Rev 2) Settings']

        checkbox_dest_orig_route = IPE_set_window.child_window(title="&Destination1, Origin1, RouteCode Present",
                                                               class_name="ThunderRT6CheckBox")

        # Checkbox - Destination, Origin, Routecode
        checkbox_dor_state = checkbox_dest_orig_route.GetCheckState()

        if checkbox_dor_state == 0:
            checkbox_dest_orig_route.Click()

        IPE_set_window.SetFocus()
        IPE_set_window.OK.Click()
        self.select_first_memory_struct()
        self.main_window.ITSO_Customer_Media_Generator.TTreeControl.type_keys('{ENTER}')

        self.CMTool_Editor = self.app[u'Editor']
        self.CMTool_Editor.TTreeControl.RightClick()

        # Set PTYP
        self.CMTool_Editor.TTreeControl0.type_keys('P')
        self.CMTool_Editor.Edit.Click()
        self.CMTool_Editor.Edit.DoubleClick()  # Double Click = Select current content
        self.CMTool_Editor.Edit.type_keys(ipe_vars.var_ptyp)
        self.CMTool_Editor.Edit.type_keys('{ENTER}')

        # Set expiry date
        self.CMTool_Editor.TTreeControl0.RightClickInput()
        self.CMTool_Editor.TTreeControl0.type_keys('EEE')
        self.CMTool_Editor.Edit.Click()
        self.CMTool_Editor.Edit.DoubleClick()  # Double Click = Select current content
        self.CMTool_Editor.Edit.type_keys('2442{ENTER}')

        # Set NLC - ORIGIN
        self.CMTool_Editor.TTreeControl2.RightClick()
        self.CMTool_Editor.TTreeControl2.type_keys('N')
        self.CMTool_Editor.Edit2.Click()
        self.CMTool_Editor.Edit2.type_keys(ipe_vars.var_orig_nlc)
        self.CMTool_Editor.Edit2.type_keys('{ENTER}')

        # Set NLC - DESTINATION
        self.CMTool_Editor.TTreeControl2.RightClick()
        self.CMTool_Editor.TTreeControl2.type_keys('NN')
        self.CMTool_Editor.Edit2.Click()
        self.CMTool_Editor.Edit2.type_keys(ipe_vars.var_dest_nlc)
        self.CMTool_Editor.Edit2.type_keys('{ENTER}')

        # ROUTE CODE - PRESS 'R' 11 TIMES OR ...
        self.CMTool_Editor.TTreeControl2.RightClick()
        self.CMTool_Editor.TTreeControl2.type_keys('RRRRRRRRRRR')
        self.CMTool_Editor.Edit2.Click()
        self.CMTool_Editor.Edit2.DoubleClick()  # Double Click = Select current content
        self.CMTool_Editor.Edit2.type_keys(ipe_vars.var_rtcode)
        self.CMTool_Editor.Edit2.type_keys('{ENTER}')

        self.CMTool_Editor.OK.ClickInput()
        self.main_window.ITSO_Customer_Media_Generator.Create_Image.Click()

        # Wait for "Create Image - Processing Finished"
        popup = self.app.Dialog
        popup.Wait('ready', timeout=6)
        popup.OK.Click()

        # Write_CM
        # Wait until some time passes so that everything is written to the card
        self.main_window.ITSO_Customer_Media_Generator.Write_CM.Click()
        time.sleep(7)

        # EDITING VALUE GROUP
        self.select_first_memory_struct()
        self.main_window.ITSO_Customer_Media_Generator.TTreeControl0.type_keys('{UP 8}{ENTER}')

        # Set Transaction Sequence Number
        self.CMTool_Editor.TTreeControl2.RightClick()
        self.CMTool_Editor.TTreeControl2.type_keys('TT')
        self.CMTool_Editor.Edit2.Click()
        self.CMTool_Editor.Edit2.DoubleClick()  # Double Click = Select current content
        self.CMTool_Editor.Edit2.type_keys('001')
        self.CMTool_Editor.Edit2.type_keys('{ENTER}')

        # Set CountRemainingRides
        self.CMTool_Editor.Click()  #to bring for window back to focus... it wouldn't work otherwise

        self.CMTool_Editor.TTreeControl2.RightClick()
        self.CMTool_Editor.TTreeControl2.type_keys('C')
        self.CMTool_Editor.Edit2.Click()
        self.CMTool_Editor.Edit2.DoubleClick()  # Double Click = Select current content
        self.CMTool_Editor.Edit2.type_keys(typ_vars.var_count_journeys_remaining)
        self.CMTool_Editor.Edit2.type_keys('{ENTER}')

        self.CMTool_Editor.OK.ClickInput()
        self.main_window.ITSO_Customer_Media_Generator.Create_Image.Click()

        # Wait for "Create Image - Processing Finished"
        popup2 = self.app.Dialog
        popup2.Wait('ready', timeout=6)
        popup2.OK.Click()

        self.main_window.ITSO_Customer_Media_Generator.Write_CM.Click()

        #WOKRS UP TO HERE

        # TODO: TREE CONTROL CHILDWINDOW FIX , make object selelction more explicit

    def create_typ24(self):
        pass
