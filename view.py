try:
    import Tkinter as Tk  # python 2
except ModuleNotFoundError:
    import tkinter as Tk  # python 3

from helper_functions import *


class View:
    def __init__(self, root, model):
        self.frame = Tk.Frame(root)
        self.model = model

        self.label_bind_status = Tk.StringVar()
        self.label_bind_status.set("UNBINDED")

        self.label_select_test = Tk.Label(self.frame, text="Choose Test:")
        self.label_select_test.grid(column=0, row=0)

    # Listbox with tests
        self.scrollbar = Tk.Scrollbar(self.frame)
        self.scrollbar.grid(column=1, row=1, sticky=Tk.N + Tk.S + Tk.W, rowspan=6)
        self.listbox_ListOfTests = Tk.Listbox(self.frame, selectmode='SINGLE', yscrollcommand=self.scrollbar.set, width=60)

        # Initialise the list with tests from a file
        self.list_of_test = read_test_file()

        for line in self.list_of_test:
            self.listbox_ListOfTests.insert(Tk.END, str(line))

        self.listbox_ListOfTests.grid(column=0, row=1, rowspan=6)
        self.scrollbar.config(command=self.listbox_ListOfTests.yview)
    # End Listbox

        self.label_binded = Tk.Label(self.frame, textvariable=self.label_bind_status, fg="red")
        self.label_binded.grid(column=2, row=0)

        self.button_test = Tk.Button(self.frame, text="TEST - Make Shell")
        self.button_test.grid(column=3, row=0, sticky=Tk.N)

        self.button_bind = Tk.Button(self.frame, text="Bind to INQ")
        self.button_bind.grid(column=2, row=1, sticky=Tk.N)

        self.button_bind_proj = Tk.Button(self.frame, text="Bind to INQ PROJ")
        self.button_bind_proj.grid(column=3, row=1, sticky=Tk.N)

        self.label_from_nlc = Tk.Label(self.frame, text="From NLC")
        self.label_from_nlc.grid(column=2, row=2, sticky=Tk.N)
        self.entry_from_nlc = Tk.Entry(self.frame)
        self.entry_from_nlc.grid(column=3, row=2, sticky=Tk.N)

        self.label_to_nlc = Tk.Label(self.frame, text="To NLC")
        self.label_to_nlc.grid(column=2, row=3, sticky=Tk.N)
        self.entry_to_nlc = Tk.Entry(self.frame)
        self.entry_to_nlc.grid(column=3, row=3, sticky=Tk.N)

        self.label_routecode = Tk.Label(self.frame, text="Routecode")
        self.label_routecode.grid(column=2, row=4, sticky=Tk.N)
        self.entry_routecode = Tk.Entry(self.frame)
        self.entry_routecode.grid(column=3, row=4, sticky=Tk.N)

        self.button_run = Tk.Button(self.frame, text="Make Card")
        self.button_run.grid(column=2, row=5)

        self.checkbutton_variable = Tk.IntVar()
        self.checkbutton_clean_card = Tk.Checkbutton(self.frame, text="Clean Card", variable=self.checkbutton_variable)
        self.checkbutton_clean_card.grid(column=3, row=5)
        # c.var = v

        #self.button_exit = Tk.Button(self.frame, text="Wipe and Add", command=self.frame.quit)
        #self.button_exit.grid(column=3, row=5)

        self.label_issn = Tk.Label(self.frame, text="ISSN:")
        self.label_issn.grid(column=2, row=6, sticky=Tk.N)
        self.entry_issn = Tk.Entry(self.frame)
        self.entry_issn.grid(column=3, row=6, sticky=Tk.N)

        self.text_logs = Tk.Text(self.frame, height=10, width=75)
        self.text_logs.grid(column=0, row=7, columnspan=4)

        self.frame.pack()

    def clear(self, event):
        self.ax0.clear()
        self.fig.canvas.draw()