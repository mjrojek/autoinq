try:
    import Tkinter as Tk # python 2
except ModuleNotFoundError:
    import tkinter as Tk # python 3

from model import Model
from view import View
from tests import *
from typ_variables import *


# TODO:
# self.view.label_bind_status.set("BINDED to PROJ")
#

class Controller:
    def __init__(self):
        self.root = Tk.Tk()
        self.model = Model()
        self.view = View(self.root, self.model)
        self.ipe_vars = IPEVariables()

        # Button Bindings
        #self.view.button_bind_proj.bind('<Button-1>', self.bind_to_project)
        self.view.button_run.bind('<Button-1>', self.run_test)
        #self.view.listbox_ListOfTests.bind('<Button-1>', self.print_test_selected)
        self.view.button_test.bind('<Button-1>', self.get_selected_test)

    def set_ipe_vars_from_entryboxes(self, event=None):
        # TODO: add validation

        self.ipe_vars.var_issn = self.view.entry_issn.get()
        self.ipe_vars.var_ptyp = self.view.entry_issn.get()
        self.ipe_vars.var_orig_nlc = self.view.entry_from_nlc.get()
        self.ipe_vars.var_dest_nlc = self.view.entry_to_nlc.get()
        self.ipe_vars.var_rtcode = self.view.entry_routecode.get()


    def test2(self, event=None):
        print(event)
        print("INSIDE RUN TEST - CHOOSING TEST BY ID")
        TestC1()

    def test3(self, event=None):
        print(event)
        TestC2()

    def run(self):
        self.root.title("INQ CM Creator - Auto v0.1")
        self.root.mainloop()

    def get_selected_test(self, event=None):
        test_index = self.view.listbox_ListOfTests.curselection()
        print("Printing Index:", test_index)

        test_name = self.view.listbox_ListOfTests.get(self.view.listbox_ListOfTests.curselection())
        print(test_name)
        test_ID = test_name.split(' ', 1)[0]
        return test_ID
        #print(self.view.listbox_ListOfTests.index(Tk.ACTIVE))
        #print(self.view.listbox_ListOfTests)
        #print(self.view.list_of_test)

    def run_test(self, event=None):

        if self.get_selected_test() == 'Staff':
            StaffPass()

        elif self.get_selected_test() == 'C280849':
            self.set_ipe_vars_from_entryboxes()
            vars_typ23 = TYP23_vars
            vars_typ23.var_count_journeys_remaining = 5
            self.ipe_vars.var_ptyp = 5
            C280849(self.ipe_vars, vars_typ23, self.view.checkbutton_variable.get())

        elif self.get_selected_test() == 'C280850':
            self.set_ipe_vars_from_entryboxes()
            vars_typ23 = TYP23_vars
            vars_typ23.var_count_journeys_remaining = 1
            self.ipe_vars.var_ptyp = 2
            C280850(self.ipe_vars, vars_typ23, self.view.checkbutton_variable.get())

        elif self.get_selected_test() == 'C280851':
            self.set_ipe_vars_from_entryboxes()
            vars_typ23 = TYP23_vars
            vars_typ23.var_count_journeys_remaining = 2
            self.ipe_vars.var_ptyp = 3
            C280851(self.ipe_vars, vars_typ23, self.view.checkbutton_variable.get())

        elif self.get_selected_test() == 'test':
            self.set_ipe_vars_from_entryboxes()
            # self.view.text_logs.insert(Tk.END,"testNEW\n")
            Test(self.ipe_vars)


        else:
            print("Case not implemented yet!")
